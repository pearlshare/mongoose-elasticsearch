mongoose = require('mongoose')

#
# Mongo setup
#
exports.connect = () ->
  mongoose.connect 'mongodb://localhost/elasticmongoosetest',
    server:
      socketOptions:
        keepAlive: 1

# Error handler
mongoose.connection.on 'error', (err) ->
  console.warn err

# Reconnect when closed
mongoose.connection.on 'disconnected', ->
  # makeMongoConnection()
  console.log('disconnected')

#
# Mongoose models
#

AnimalSchema =
  new mongoose.Schema
    name:
      type: String
    description:
      type: String
    createdAt:
      type: Date

exports.AnimalSchema = AnimalSchema