elasticsearch = require('elasticsearch')

#
# Elasticsearch setup
#
exports.client = new elasticsearch.Client
  host: 'localhost:9200'
  maxSockets: 2
  log:
    levels: ['error', 'warning', 'info']#, 'debug', 'trace']

exports.index = 'animals'

#
# Fish Class
#
exports.animalMapping =
  properties:
    name:
      type: 'string'
    description:
      type: 'string'
      search_analyzer: 'snowball'
      index_analyzer: 'snowball'
    createdAt:
      type: 'date'