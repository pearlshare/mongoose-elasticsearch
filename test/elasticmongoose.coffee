
helper = require('./helper')

mongoose = require('mongoose')
Promise = require('bluebird')
ElasticMongoose = require('../index')


#
# Elasticsearch setup
#
elasticClient = helper.elasticsearch.client
elasticIndex = helper.elasticsearch.index
elasticMongoose = new ElasticMongoose
  index: elasticIndex
  client: elasticClient

#
# Fish Class
#
FishSchema = helper.mongo.AnimalSchema
animalMapping = helper.elasticsearch.animalMapping

# Add the plugin giving the type and
FishSchema.plugin elasticMongoose.plugin(),
  type: 'fish'
  mapping: animalMapping
  toObjectMethod: 'toSearchObject'

# Mock method to test custom toObject with a delayed promise
FishSchema.methods.toSearchObject = (callback) ->
  buildObj = () => @toObject()
  Promise.delay(50).then(buildObj).nodeify(callback)

Fish = mongoose.model "Fish", FishSchema


#
# Mamal Class
#
MamalSchema = helper.mongo.AnimalSchema

# Add the plugin giving the type and
MamalSchema.plugin elasticMongoose.plugin(),
  type: 'mamal'
  mapping: animalMapping

Mamal = mongoose.model "Mamal", MamalSchema

checkIndex = () ->
  elasticMongoose.indexExists()

removeIndex = (bool) ->
  if bool
    elasticMongoose.deleteIndex()
  else
    Promise.resolve(true)

createIndex = () ->
  elasticMongoose.connect()

putMappings = ->
  tasks = [
    Fish.putMapping()
    Mamal.putMapping()
  ]
  Promise.all(tasks)

addAnimals = ->
  fish1 = new Fish name: 'Whale', description: 'The biggest fish like thing in the sea'
  fish2 = new Fish name: 'Sail fish', description: 'A fast fish'
  fish3 = new Fish name: 'Great white shark', description: "It'll get ya!"
  mamal1 = new Mamal name: 'Monkey', description: 'Cheeky chappy'
  mamal2 = new Mamal name: 'Squirrel', description: 'Known for climbing trees and trumping'

  tasks = [
    fish1.saveAsync().get(0)
    fish2.saveAsync().get(0)
    fish3.saveAsync().get(0)
    mamal1.saveAsync().get(0)
    mamal2.saveAsync().get(0)
  ]
  Promise.all(tasks)


#
# Start tests
#
before ->
  helper.mongo.connect()


describe 'ElasticMongoose', ->


  context "Index", ->
    before (done) ->
      createIndex().delay(500).nodeify(done)

    describe 'putMapping', ->
      it 'should put the mapping to the index', ->
        Fish.putMapping().then (resp) ->
          expect(resp.acknowledged).to.equal(true)

      it 'should put the mapping to the index', ->
        Mamal.putMapping().then (resp) ->
          expect(resp.acknowledged).to.equal(true)

  context "Collections", ->

    describe "syncSearchIndex", ->
      @timeout(15000)

      before ->
        addAnimals().then(checkIndex).then(removeIndex).delay(500).then(createIndex).delay(500).then(putMappings).delay(500)

      it 'should index all Fish documents', ->
        Fish.syncSearchIndex()

      it 'should index all Mamal documents', ->
        Mamal.syncSearchIndex()

    describe "search", ->
      @timeout(15000)

      before (done) ->
        checkIndex().then(removeIndex).delay(500).then(createIndex).delay(500).then(putMappings).then(addAnimals).delay(500).nodeify(done)

      it 'should find 2 documents with the keyword fish', ->
        promise = Fish.search
          query:
            match:
              description: 'fish'

        promise.then (results) ->
          expect(results.hits.hits).to.have.length(2)

      it 'should find 1 mamal with the keyword tree (stemmed trees)', ->
        promise = Mamal.search
          query:
            match:
              description: 'tree'

        promise.then (results) ->
          expect(results.hits.hits).to.have.length(1)

  context "Documents", ->
    elephant = new Mamal name: 'Elephant', description: 'The biggest mamal not in the sea'

    saveElephant = () ->
      elephant.saveAsync().get(0)

    findElephant = (elephant) ->
      elasticClient.get
        index: elasticIndex
        type: 'mamal'
        id: "#{elephant.id}"

    checkElephantInIndex = (resp) ->
      expect(resp._id).to.equal(elephant.id)

    removeElephant = () ->
      elephant.remove()

    checkElephantNotInIndex = (err) ->
      expect(err.message).to.equal('Not Found')

    describe 'test model', ->
      it 'should have the name "Elephant"', ->
        expect(elephant).to.have.property('name')
        expect(elephant.name).to.equal('Elephant')

    describe 'when saving the elephant', ->
      it 'should also save to elasticsearch', ->
        saveElephant().then(findElephant).then(checkElephantInIndex)

    describe 'when removing a document', ->
      it 'should also remove from elasticsearch', ->
        saveElephant()
          .then(findElephant)
          .then(checkElephantInIndex)
          .then(removeElephant)
          .then(findElephant)
          .catch(checkElephantNotInIndex)
