###
  Module dependencies.
###
chai = require('chai')

Promise = require('bluebird')
mongoose = require('mongoose')
Promise.promisifyAll(mongoose)

###
  Globals
###
# Add chai should helper to object prototype
global.expect = chai.expect

exports.mongo = require('./support/mongoose')

exports.elasticsearch = require('./support/elasticsearch')
