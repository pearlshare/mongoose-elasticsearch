Promise = require('bluebird')
ElasticMongooseModel = require('./model')

###
  ElasticMongoose is  a wrapper around the elasticsearch object using a promise library and the default elaticsearch options.
  Options: http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-conventions.html
  A log method can also be specified for logging errors
###
class ElasticMongoose

  ###
    new ElasticMongoose - setup an elasticMongooseQ instance

    @param {Object} options hash
      @option {String} index - default elasticsearch index to use
      @option {Object} client - configured elasticsearch client to use
      @option {Object} promise - promise library to use (bluebird/Q)
      @options {Object} logger - log function (defaults to console)
  ###
  constructor: (options) ->
    @elasticClient = options.client
    @elasticIndex = options.index
    @elasticTypes = []
    @promise = options.promise || Promise
    @logger = options.logger || console

  connect: (callback) ->
    self = this

    createIfMissing = (boolean) ->
      if boolean then Promise.resolve(true) else self.createIndex()

    @indexExists().then(createIfMissing).nodeify(callback)


  #
  # Index Management
  #

  ###
    exists
    http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-reference.html#api-indices-create
    @param {Function} callback - callback with node signature
    @returns {Promise}
  ###
  indexExists: (callback) ->
    @elasticClient.indices.exists(index: @elasticIndex).nodeify(callback)

  ###
    createIndex
    http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-reference.html#api-indices-create
    @param {Function} callback - callback with node signature
    @returns {Promise}
  ###
  createIndex: (callback) ->
    @elasticClient.indices.create(index: @elasticIndex).nodeify(callback)

  ###
    refreshIndex
    http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-reference.html#api-indices-delete
    @param {Function} callback - callback with node signature
    @returns {Promise}
  ###
  refreshIndex: (callback) ->
    @elasticClient.indices.refresh(index: @elasticIndex).nodeify(callback)

  ###
    deleteindex
    http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-reference.html#api-indices-delete
    @param {Function} callback - callback with node signature
    @returns {Promise}
  ###
  deleteIndex: (callback) ->
    @elasticClient.indices.delete(index: @elasticIndex).nodeify(callback)

  ###
    clearIndex - removes the configured elasticsearch index
    http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-reference.html#api-deletebyquery
    @param {Function} callback - callback with node signature
    @returns {Promise}
  ###
  clearIndex: (callback) ->
    opts =
      index: @elasticIndex
      type: @elasticTypes
      q: '*'
    @elasticClient.deleteByQuery(opts).nodeify(callback)


  #
  # Mongoose plugin
  #

  ###
    Plugin - returns a function suitable for creating a mongoose plugin
  ###
  plugin: ->
    elasticMongooseModel = new ElasticMongooseModel(this)
    elasticMongooseModel.plugin


module.exports = ElasticMongoose
